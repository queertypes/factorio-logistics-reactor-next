data:extend({
      {
         type = "technology",
         name = "logistic-reactor",
         icon = "__logistics-reactor-next__/graphics/icons/logistic-reactor.png",
         icon_size = 128,
         effects =
            {
               {
                  type = "unlock-recipe",
                  recipe = "logistic-reactor"
               }
            },
         prerequisites = {"nuclear-power", "logistic-system"},
         unit =
            {
               count = 150,
               ingredients =
                  {
                     {"automation-science-pack", 1},
                     {"chemical-science-pack", 1},
                     {"utility-science-pack", 1},
                     {"logistic-science-pack", 1}
                  },
               time = 30,
               order = "e-p-b-d"
            },
      }
})

if settings.startup["bobmods-power-nuclear"] and
   settings.startup["bobmods-power-nuclear"].value == true then

   data:extend({
         {
            type = "technology",
            name = "logistic-reactor-2",
            icon = "__logistics-reactor-next__/graphics/icons/logistic-reactor.png",
            icon_size = 128,
            effects =
               {
                  {
                     type = "unlock-recipe",
                     recipe = "logistic-reactor-2"
                  }
               },
            prerequisites = {"bob-nuclear-power-2", "logistic-system-2", "logistic-reactor"},
            unit =
               {
                  count = 250,
                  ingredients =
                     {
                        {"automation-science-pack", 1},
                        {"chemical-science-pack", 1},
                        {"utility-science-pack", 1},
                        {"logistic-science-pack", 1}
                     },
                  time = 30,
                  order = "e-p-b-d"
               }
         },

         {
            type = "technology",
            name = "logistic-reactor-3",
            icon = "__logistics-reactor-next__/graphics/icons/logistic-reactor.png",
            icon_size = 128,
            effects =
               {
                  {
                     type = "unlock-recipe",
                     recipe = "logistic-reactor-3"
                  }
               },
            prerequisites = {"bob-nuclear-power-3", "logistic-system-3", "logistic-reactor-2"},
            unit =
               {
                  count = 500,
                  ingredients =
                     {
                        {"automation-science-pack", 1},
                        {"chemical-science-pack", 1},
                        {"utility-science-pack", 1},
                        {"logistic-science-pack", 1}
                     },
                  time = 30,
                  order = "e-p-b-d"
               }
         }
   })
end
