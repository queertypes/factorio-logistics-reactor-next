---------------------------------------------------------------------------------------------------
Version: 6.0.0
Date: September 30, 2021
  Major Features:
    - Bump version requirements to enable compatibility with Factorio 1.1.
---------------------------------------------------------------------------------------------------
Version: 5.0.1
Date: January 25, 2020
  Bugfixes:
    - Correctly support conditional loading of Bob's dependencies.
---------------------------------------------------------------------------------------------------
Version: 5.0.0
Date: January 25, 2020
  Major Features:
    - Bump version requirements to enable compatibility with Factorio 0.18.
---------------------------------------------------------------------------------------------------
Version: 4.0.0
Date: September 23, 2019
  Major Features:
    - Rewrote reactor recipes to be more upgrade friendly. Now, a
      logistics reactor is: a reactor + a passive chest + a request
      chest. Don't need to rebuild from scratch!
    - Added compatibility with Bob's Power - tier 2 and tier 3
      reactors. Also added appropriate research techs and recipes.
  Bugfixes:
    - on_nth_tick hanlders for refueling and counting reactors could
      conflict. If this was the case, only the latter was set up and
      reactors would never refuel. Made a small tweak so that they'll
      never overlap now, regardless of user provided refuel interval.
    - Would occasionally crash when deconstructed. Should never crash now.
---------------------------------------------------------------------------------------------------
Version: 3.0.1
Date: September 3, 2019
  Major Features:
    - Improved compatibility with all forms of nuclear fuel cells.
      Specifically made compatible with breeder fuel cells from Gotlag's Nuclear Fuel mod.
  Info:
    - Reformatted existing changelog.md into Factorio-readable changelog.txt
      Added Kryzeth to list of mod authors for this update.
  Bugfixes:
    - Fixed the timer setting from 10 minutes to the intended 10 seconds.
---------------------------------------------------------------------------------------------------
Version: 2.2.0
Date: September 15, 2018
  Minor Features:
    - made refuel/waste-handling rate configurable, with a 10 second default.
---------------------------------------------------------------------------------------------------
Version: 2.1.0
Date: September 15, 2018
  Bugfixes:
    - Fixed crash when deconstructing reactor.
  Changes:
    - Restructured mod code to be somewhat more inline with typical mod organization
---------------------------------------------------------------------------------------------------
Version: 2.0.0
Date: September 15, 2018
  Info:
    - Updated mod for 0.16
  Changes:
    - Reduced replenish rate from once per second to once per 10 seconds
  Bugfixes:
    - Ensure items are retrieved from attached storages before deconstructing
