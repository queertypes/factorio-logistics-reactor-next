data:extend({
      {
         type = "int-setting",
         name = "logistics-reactor-next-refuel-seconds",
         setting_type = "startup",
         default_value = 10,
         minimum_value = 1,
         maximum_value = 1000
      }
})
