data:extend({
      {
         type = "item",
         name = "logistic-reactor",
         icon = "__base__/graphics/icons/nuclear-reactor.png",
         icon_size = 32,
         subgroup = "energy",
         order = "f[nuclear-energy]-a[reactor]",
         place_result = "logistic-reactor",
         stack_size = 10
      }
})

if settings.startup["bobmods-power-nuclear"] and
   settings.startup["bobmods-power-nuclear"].value == true then

   data:extend({
         {
            type = "item",
            name = "logistic-reactor-2",
            icon = "__base__/graphics/icons/nuclear-reactor.png",
            icon_size = 32,
            subgroup = "energy",
            order = "f[nuclear-energy]-a[reactor-2]",
            place_result = "logistic-reactor-2",
            stack_size = 10
         },
         {
            type = "item",
            name = "logistic-reactor-3",
            icon = "__base__/graphics/icons/nuclear-reactor.png",
            icon_size = 32,
            subgroup = "energy",
            order = "f[nuclear-energy]-a[reactor-3]",
            place_result = "logistic-reactor-3",
            stack_size = 10
         }
   })

end
